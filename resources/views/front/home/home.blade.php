@extends('front.master')
@section('tilte_site', 'Trang chủ')
@section('content')
<div class="container-fluid">
        <div class="container">
                <div class="owl-carousel owl-theme main-slide">
                        <div class="item">
                                <img src="https://media.vinastudy.vn/sliders/22/22.jpg" alt="">
                        </div>
                        <div class="item">
                                <img src="https://media.vinastudy.vn/sliders/22/22.jpg" alt="">
                        </div>
                        <div class="item">
                                <img src="https://media.vinastudy.vn/sliders/22/22.jpg" alt="">
                        </div>
                </div>
        </div>
</div>
@endsection
@push('scripts')
    <script>
            $( document ).ready(function() {
                    console.log(1);
                $('.main-slide').owlCarousel({
                        loop:true,
                        margin:10,
                        nav:true,
                        responsive:{
                                0:{
                                items:1
                                },
                                600:{
                                items:3
                                },
                                1000:{
                                items:5
                                }
                        }
                })
});
                
    </script>
@endpush