<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function showHomeAdmin(){
        return view('admin.master',[
            'name'=>'hello'
        ]);
    }
    public function showLoginAdmin(){
        return view('admin.login',[
            'name'=>'login'
        ]);
    }
    public function showHome(){
        return view('front.home.home',[
            'name'=>'home'
        ]);
    }
}
